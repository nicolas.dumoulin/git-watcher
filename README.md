# git-watcher

Watch a git repository, fetch commits and post a summary on a mattermost chat.

The script will start a selenium session on a mattermost chat, and then watch
on a git repository for a commit. As a commit will appear, it will be notified
in the chat.

## Usage

Type ```python3 git-watcher.py -h``` for usage and options documentation:
```
usage: git-watcher.py [-h] [-l GITLAB] [-m MATTERMOST] [-s SLEEP] [gitrepo] [localrepo]

Fetch commits on a git repository and post summary on mattermost.

positional arguments:
  gitrepo               URL of the GIT repository
  localrepo             local repository to create/use

optional arguments:
  -h, --help            show this help message and exit
  -l GITLAB, --gitlab GITLAB
                        URL of the gitlab for linking to the commits into messages
  -m MATTERMOST, --mattermost MATTERMOST
                        URL of the mattermost where messages will be posted. Without this option, messages will be posted on console output.
  -s SLEEP, --sleep SLEEP
                        Duration (in seconds) of the sleep between 2 fetching operations (by default: 60)
```

### Examples

```
python3 git-watcher/git-watcher.py 'https://forgemia.inra.fr/agriterix/simulator.git' '/home/dumoulin/tmp/agriterix-simulator2'
```
It will clone the git repository into the specified directory, or load this
local directory if it is already a git repository, and print the commits on the
console output.

```
python3 git-watcher/git-watcher.py 'https://forgemia.inra.fr/agriterix/simulator.git' '/home/dumoulin/tmp/agriterix-simulator2' --gitlab 'https://forgemia.inra.fr/agriterix/simulator/-/commit/' --mattermost 'https://team.forgemia.inra.fr/agriterix/channels/commits'
```
It will to do the same as previous example, but it will post the commits messages
in the chat indicated by the option ```--mattermost```. The option ```--gitlab```
will add links to the webpage of the commit into the message.
