#!/usr/bin/python3
# -*- coding: utf-8 -*-

import os
import pygit2
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

def log(message):
    print(time.strftime('%d/%m/%Y %H:%M:%S',time.localtime()) + " " + str(message))

class RepoWatcher:
    def __init__(self, repo_url, repo_dir, gitlab_url=None, mattermost_url=None):
        self.gitlab_url= gitlab_url
        self.mattermost_url = mattermost_url
        if not os.path.isdir(repo_dir) or not os.path.isdir(os.path.join(repo_dir,'.git')):
            log('Cloning repository from ' + repo_url)
            self.repo = pygit2.clone_repository(repo_url, repo_dir)
        else:
            self.repo = pygit2.Repository(repo_dir)
        self.last_commit_hex = self.repo[self.repo.head.target].hex
        if mattermost_url:
            self.init_selenium()
            self.get_mattermost_post_element()

    def init_selenium(self):
        if self.mattermost_url:
            self.driver = webdriver.Firefox()
            self.driver.base_url = self.mattermost_url
            self.driver.get(self.driver.base_url)

    def get_last_commits(self):
        self.repo.remotes[0].fetch()
        commits=[]
        last_commit = self.repo[self.repo.remotes['origin'].ls_remotes()[0]['oid']]
        if last_commit.hex != self.last_commit_hex:
        #if self.repo[self.repo.head.target].hex != self.last_commit_hex:
            #for commit in repo.repo.walk(repo.repo.head.target, pygit2.GIT_SORT_TIME):
            for commit in repo.repo.walk(self.repo.remotes['origin'].ls_remotes()[0]['oid'], pygit2.GIT_SORT_TIME):
                if commit.hex == self.last_commit_hex:
                    self.last_commit_hex = last_commit.hex
                    break
                commits.append(commit)
        return commits

    def str_format_commits(self,commits):
        s = ""
        for commit in commits:
            if self.gitlab_url:
                s += "["
            s += time.strftime('%d/%m/%Y %H:%M:%S',time.localtime(commit.commit_time))
            if self.gitlab_url:
                s += "](" + self.gitlab_url + commit.hex + ")"
            s += " " + str(commit.author.name)
            s += "\n"
            s += commit.message + "\n"
        return s

    def get_mattermost_post_element(self):
        success = False
        counter = 0
        pause_time = 5
        time_limit = int(360 / pause_time) # 30 minutes
        while not success:
            try:
                return self.driver.find_element_by_xpath('//*[@id="post_textbox"]')
            except:
                time.sleep(pause_time)
                counter += 1
                log("Mattermost Web page not ready")
                if counter % time_limit == 0: # Every 30 minutes
                    log("Selenium session lost. Restarting a new one.")
                    self.init_selenium()

    def post_via_selenium(self, message):
        messageElt = self.get_mattermost_post_element()
        messageElt.send_keys(message)
        messageElt.submit()

    def post_commits_selenium(self, commits):
        messageElt = self.get_mattermost_post_element()
        messageElt.send_keys(self.str_format_commits(commits))
        messageElt.submit()

    def watch(self, time_sleep=60):
        while True:
            message = self.str_format_commits(self.get_last_commits())
            if self.mattermost_url:
                self.post_via_selenium(message)
            else:
                print(message)
            time.sleep(time_sleep)

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser(description='Fetch commits on a git repository and post summary on mattermost.')
    parser.add_argument('gitrepo', nargs='?',
                        help='URL of the GIT repository')
    parser.add_argument('localrepo', nargs='?',
                        help='local repository to create/use')
    parser.add_argument('-l', '--gitlab', default=None,
                        help='URL of the gitlab for linking to the commits into messages')
    parser.add_argument('-m', '--mattermost',default=None,
                        help='URL of the mattermost where messages will be posted. Without this option, messages will be posted on console output.')
    parser.add_argument('-s', '--sleep', default=60,
                        help='Duration (in seconds) of the sleep between 2 fetching operations (by default: 60)')
    args = parser.parse_args()
    repo=RepoWatcher(args.gitrepo, args.localrepo, args.gitlab, args.mattermost)
    repo.watch(int(args.sleep))
